<?php

namespace App\Tests\Unit\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\EventSubscriber\UserRegisterSubscriber;
use Codeception\Test\Unit;
use Symfony\Component\HttpKernel\KernelEvents;

class UserRegisterSubscriberTest extends Unit
{
    public function testConfiguration()
    {
        $result = UserRegisterSubscriber::getSubscribedEvents();

        $this->assertArrayHasKey(KernelEvents::VIEW, $result);
        $this->assertEquals(
            ['userRegister', EventPriorities::PRE_WRITE],
            $result[KernelEvents::VIEW]
        );
    }
}
