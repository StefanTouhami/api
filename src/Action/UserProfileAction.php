<?php

namespace App\Action;

use App\Entity\User;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UserProfileAction extends AbstractController
{
    /**
     * @Route(
     *     "/api/user/profile",
     *     name="api_user_profile_get_item",
     *     methods={"GET"}
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns logged user data",
     *     @Model(type=User::class, groups={"get"})
     * )
     * @SWG\Tag(name="User")
     */
    public function profile(SerializerInterface $serializer): JsonResponse
    {
        $user = $serializer->normalize($this->getUser(), 'array');
        return new JsonResponse($user);
    }
}
