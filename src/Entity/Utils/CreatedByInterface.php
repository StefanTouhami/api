<?php

namespace App\Entity\Utils;

use Symfony\Component\Security\Core\User\UserInterface;

interface CreatedByInterface
{
    public function setCreatedBy(UserInterface $user): self;
}
