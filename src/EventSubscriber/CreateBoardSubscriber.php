<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Board;
use App\Entity\User;
use Cocur\Slugify\SlugifyInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateBoardSubscriber implements EventSubscriberInterface
{
    private UserPasswordEncoderInterface $passwordEncoder;
    /**
     * @var SlugifyInterface
     */
    private SlugifyInterface $slugify;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        SlugifyInterface $slugify
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->slugify = $slugify;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['board', EventPriorities::PRE_WRITE]
        ];
    }

    public function board(ViewEvent $event): void
    {
        /** @var Board $object */
        $object = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$object instanceof Board || !in_array($method, [Request::METHOD_POST, Request::METHOD_PUT])) {
            return;
        }

        $slug = $this->slugify->slugify($object->getName());
        $object->setSlug($slug);
    }
}
